    ///////////////////////////////////////////////////////////////
    // Thomas Buxó                                               //
    ///////////////////////////////////////////////////////////////

    var updateOppervlakte = function(im) {

        // Sla svg textblokken in een verzameling
        var tekstBlokken = im.querySelector("svg").getElementsByTagName("text");

        // Sla afmetingen van svg op in variabelen
        var frameW = im.offsetWidth;
        var frameH = im.offsetHeight;

        // Bereken oppervlakte afbeelding
        var frameSurface = frameW * frameH;

        // Bereken oppervlakte window
        var pageSurface = window.innerWidth * window.innerHeight;

        // Bereken ratio van oppervlaktes afbeelding t.o.v. viewport
        var updatedFrameSurface = parseFloat((frameSurface * 100) / pageSurface).toFixed(2);
        // Opsplitsen in getallen voor- en na de comma
        var integers = updatedFrameSurface.toString().split(".");
        var voorComma = integers[0];
        var naComma = integers[1].substring(0, 2);

        // Vervang default waarden in tekstblokken met geüpdate waarden
        tekstBlokken[0].querySelector("tspan").innerHTML = voorComma;
        tekstBlokken[1].querySelector("tspan").innerHTML = naComma;

        // Default waarden
        tekstBlokken[0].querySelector("tspan").setAttribute("x", "310");
        tekstBlokken[0].querySelector("tspan").setAttribute("letter-spacing", "-2");
        tekstBlokken[2].querySelector("tspan").setAttribute("x", "20");
        tekstBlokken[2].querySelector("tspan").setAttribute("y", "120");
        tekstBlokken[0].setAttribute("transform", "matrix(1.6653345369377348e-16 -0.9999999999999991 0.9999999999999998 1.6653345369377353e-16 -47.28329809725159 347.061310782241)");
        tekstBlokken[0].setAttribute("font-size", "235");
        tekstBlokken[2].querySelector("tspan").innerHTML = "Off";
        tekstBlokken[2].querySelector("tspan").setAttribute("letter-spacing", "-5");

        // Firefox hack
        if (navigator.userAgent.search("Firefox") >= 0) {
            // Vóór de comma
            tekstBlokken[0].setAttribute("textLength", "265");
            tekstBlokken[0].querySelector("tspan").setAttribute("x", "305");
            // Na de comma
            tekstBlokken[1].setAttribute("textLength", "155");
        }

        if (voorComma > 99) {
            // Bij drie cijferige nummers

            // Blok “Off” verlagen
            tekstBlokken[2].querySelector("tspan").setAttribute("y", "230");

            // Firefox hack voor letterspatiëring
            if (navigator.userAgent.search("Firefox") >= 0) {
                tekstBlokken[0].setAttribute("textLength", "370");
            }

            // “Off” vervangen door “Extra” -- Bug: verdubbeld overlappende textblook
            // tekstBlokken[2].querySelector("tspan").innerHTML = "Xtra";

            // Firefox hack
            if (navigator.userAgent.search("Firefox") >= 0) {
                tekstBlokken[2].setAttribute("textLength", "185");
            }

        } else if (voorComma < 10) {
            // Bij cijfers tussen 0 en 9

            tekstBlokken[0].setAttribute("transform", "translate(-90, 40)");
            tekstBlokken[0].setAttribute("font-size", "360");
        }

    };

    // CPU besparende resize
    // https://developer.mozilla.org/en-US/docs/Web/Events/resize
    (function() {
        var throttle = function(type, name, obj) {
            obj = obj || window;
            var running = false;
            var func = function() {
                if (running) {
                    return;
                }
                running = true;
                requestAnimationFrame(function() {
                    obj.dispatchEvent(new CustomEvent(name));
                    running = false;
                });
            };
            obj.addEventListener(type, func);
        };

        // init event
        throttle("resize", "optimizedResize");
    })();

    var thomasAjax = function(elmt) {
        var svgUrl =  window.location.protocol + '//' + window.location.hostname + '/site/assets/files/1098/thomas-buxo.svg';
        var ajax = new XMLHttpRequest();
        ajax.open("GET", svgUrl, true);
        // ajax.open("GET", "http://localhost:8888/koetsier/site/assets/files/1098/thomas-buxo.svg", true);
        ajax.send();
        ajax.onload = function(e) {
            var svgData = ajax.responseText;
            var bijschrift = elmt.querySelector(".caption");
            elmt.innerHTML = svgData;
            if (bijschrift !== null) {
                elmt.appendChild(bijschrift);
            };
            // Update waarden vóór resize
            updateOppervlakte(elmt);
            // Update na resize
            window.addEventListener("optimizedResize", function() {
                updateOppervlakte(elmt);
            });
        };
    };

    var beelden = document.querySelectorAll("img");
    for (n = 0; n < beelden.length; n++) {
        if (beelden[n].src.search("thomas-buxo") > -1) {
            var thomasBeeld = beelden[n].parentNode;
            console.log(thomasBeeld);
            thomasAjax(thomasBeeld);
        }
    };